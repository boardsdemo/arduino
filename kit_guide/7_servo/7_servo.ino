// 7_servo
// rotate the servo motor between 0 to 180 degree

#include <Servo.h>
Servo servo_port;

void setup() {
  servo_port.attach(3);
}

void loop() {
//  servo_port.write(45);  // 45 degree
//  delay(1000);
//  servo_port.write(90);
//  delay(1000);

  for (int i=0; i<180; i++){
    servo_port.write(i);
    delay(10);
  }

  for (int i=180; i>0; i--){
    servo_port.write(i);
    delay(10);
  } 
}
