// 12_transistor
// user transistor as switch

const int transistor = 2;

void setup() {
  pinMode(transistor, OUTPUT);
}

void loop() {
  digitalWrite(transistor, HIGH);  // on LED
  delay(1000);
  digitalWrite(transistor, LOW);  // off LED
  delay(1000);
}
