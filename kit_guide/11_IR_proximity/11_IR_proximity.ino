// 11_IR_proximity
// Calculate distance using IR sensors
// move paper up and down and see the reading. 
// closer the paper, higher will be the number. 

// change the polarity of photo_diode in the actual document


const int photo_diode = A0;
int input_val = 0; // store input value from LM35

void setup() {
  Serial.begin(9600); 
}

void loop() {
  input_val = analogRead(photo_diode); 
  Serial.print("Input value: ");
  Serial.println(input_val);
  delay(1000);
}
