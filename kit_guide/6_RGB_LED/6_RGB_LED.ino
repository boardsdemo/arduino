// 6_RGB_LED
// display differnt color on 4 pin RGB LED. 

const int RED = 9;
const int GREEN = 10; 
const int BLUE = 11; 
int i = 0;
int j = 0;
int k = 0;

void setup() {
  pinMode(RED, OUTPUT);
  pinMode(GREEN, OUTPUT);
  pinMode(BLUE, OUTPUT);
}

void loop() {
  analogWrite(RED, 250);
  analogWrite(GREEN, 0);
  analogWrite(BLUE, 0);
  delay(500);  
    
  analogWrite(RED, 0);
  analogWrite(GREEN, 250);
  analogWrite(BLUE, 0);
  delay(500);
  
  analogWrite(RED, 0);
  analogWrite(GREEN, 0);
  analogWrite(BLUE, 250);
  delay(500);
  
  analogWrite(RED, 250);
  analogWrite(GREEN, 250);
  analogWrite(BLUE, 0);
  delay(500);  
  
  analogWrite(RED, 250);
  analogWrite(GREEN, 0);
  analogWrite(BLUE, 250);
  delay(500);

  analogWrite(RED, 0);
  analogWrite(GREEN, 250);
  analogWrite(BLUE, 250);
  delay(500);
    
  analogWrite(RED, 150);
  analogWrite(GREEN, 111);
  analogWrite(BLUE, 50);
  delay(500);

    
  analogWrite(RED, 250);
  analogWrite(GREEN, 120);
  analogWrite(BLUE, 230);
  delay(500);

    
  analogWrite(RED, 100);
  analogWrite(GREEN, 125);
  analogWrite(BLUE, 205);
  delay(500);  
}
