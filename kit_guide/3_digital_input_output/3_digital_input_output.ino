// 3_digital_input_output
// read input button value and turn on/off LED based on it

const int button = 2; 
const int LED = 3;
int buttonState = 0; 

void setup() {
  pinMode(LED, OUTPUT);
  pinMode(button, INPUT); 
}

void loop() {
  buttonState = digitalRead(button); // read digital input
  if (buttonState == HIGH)
    digitalWrite(LED, HIGH);
  else
    digitalWrite(LED, LOW);
}
