// 5_analog input
// analog input can be read from the port A0-A5 which have ADC. 
// ADC is 10 bit wide, therefore range is 0-1024
// PWM is 8 bit wide therefore range is 0-255. 
// Therefore to send the ADC output to PWM ports we need to devide it by 4. 

// in this example, input is read from analog port and sent to PWM ports. 

const int analog_input = A0;
const int LED = 3; 
int inputVal = 0;

void setup() {
  pinMode(LED, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  inputVal = analogRead(analog_input); // read input from analog port
  Serial.print("ADC: ");
  Serial.print(inputVal);
  
  Serial.print("  PWM: ");
  Serial.println(inputVal/4); 
  analogWrite(LED, inputVal/4); // divide ADC value by 4
  delay(500);  
}
