// 4_serial_monitor
// read input and output from arduino using serial cable. 
// no special hardware required, on usb connection required

// to view output Tools->Serial monitor. (or press ctrl+shift+m

int i = 0;

void setup() {
  Serial.begin(9600); // setup baud rate; should be defined in setup()
  Serial.print("Hello World\n");
}

void loop() {
  Serial.print("Counting\n");
  for (i=0; i<10; i++){
    Serial.println(i);  
    delay(1000);
  }
}
