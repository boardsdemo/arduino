// 8_LCD
// LCD connection

#include <LiquidCrystal.h>
LiquidCrystal MyLCD(12, 11, 5, 4, 3, 2); // Reset, enable, data4, data5, data6, data7

void setup() {
  MyLCD.begin(16, 2); // 16x2 LCD
  MyLCD.home(); // set cursor to row:0, col:0
  MyLCD.print("Hello World!");
  MyLCD.setCursor(0, 1); // (col, row): set cursor to col: 0, row: 1
  MyLCD.print("- Meher Krishna"); 
}

void loop() {

}
