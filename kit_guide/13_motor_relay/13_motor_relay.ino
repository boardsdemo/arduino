// 13_motor_relay
// use relay to drive motor

// if problem in loading code, remove wire from pin 2;
// load the code and again connect the wire at pin 2. 

// transitor is used to turn on/off the relay
// then relay drive the motor

const int relay = 2;

void setup() {
  pinMode(relay, OUTPUT);
}

void loop() {
  digitalWrite(relay, HIGH);  // on Relay
  delay(1000);
  digitalWrite(relay, LOW);  // off Relay
  delay(1000);
}
