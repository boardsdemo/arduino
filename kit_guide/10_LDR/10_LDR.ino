// 10_LDR
// Light dependent resistor. 
// Resistance increases with increase in light

const int LDR = A0;
int input_val = 0; // store input value from LM35

void setup() {
  Serial.begin(9600); 
}

void loop() {
  input_val = analogRead(LDR); 
  Serial.print("LDR value: ");
  Serial.println(input_val);
  delay(1000);
}
