// 15_buzzer
// turn off and on buzzer

const int buzzer = 2;

void setup() {
  pinMode(buzzer, OUTPUT);
}

void loop() {
  digitalWrite(buzzer, HIGH);  // on buzzer
  delay(100);
  digitalWrite(buzzer, LOW);  // off buzzer
  delay(1000);
}
