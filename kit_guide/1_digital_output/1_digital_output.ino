// 1_digital_output
// Blink LED at a rate of LEDRate
// connect board to port; and select the port from Tools->port

const int LED = 2; // LED Port
const int LEDrate = 500; // blinking rate

// setup run once only
void setup() {
  pinMode (LED, OUTPUT); // set LED as output port
}

// loop runs forever
void loop() {
  digitalWrite(LED, HIGH);  // digital write
  delay(LEDrate);
  digitalWrite(LED, LOW);
  delay(LEDrate);
}
