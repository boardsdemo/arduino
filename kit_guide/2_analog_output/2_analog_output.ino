// 2_analog_output
// Blink LED at a rate of LEDRate

// if connected to 5V then analog output can be any value between 0 to 5
// PWM is used for analog output; analog output is drawn in range 0 to 255. 

const int LED = 3; // LED Port

void setup() {
  // put your setup code here, to run once:
  pinMode (LED, OUTPUT); // set LED as output port
}

void loop() {
  for (int brightness=0; brightness<256; brightness++){
    analogWrite(LED, brightness); // analog write : value between 0 to 255
    delay(5);
  }
  
  for (int brightness=255; brightness>=0; brightness--){
    analogWrite(LED, brightness);
    delay(5);
  }
}
